<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Testing Email</title>
</head>
<body>
    <h1>{{ $details['title'] }}</h1>
    <h3>{{ $details['body'] }}</h3>
    <p>Terima kasih</p>
</body>
</html>