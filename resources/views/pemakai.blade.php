@extends('layouts.main')

@section('title', 'Pemakai | Official Latihan 1')
    
@section('container')
    <div class="container">
        {{-- {{ dd($data[1]["title"]); }} --}}
        @foreach ($data as $d)
            <h1>Judul : {{ $d["title"] }}</h1>
            <h3>Author : {{ $d["author"] }}</h3>
            <p>{{ $d["teks"] }}</p>    
        @endforeach
    </div>
@endsection