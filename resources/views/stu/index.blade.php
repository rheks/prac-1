@extends('layouts.main')

@section('title', 'Stu | Official Latihan 1')

@section('container')
    {{-- @dd($data); --}}

    <div class="container mt-5">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <a href="/stu/create" class="btn btn-primary">Tambah Data Blog</a>
        @foreach ($data as $d)
            <h1>Judul : {{ $d->judul }}</h1>
            <h3>Penulis : {{ $d->penulis }}</h3>
            <p>{{ \Str::limit($d->teks, 100) }}</p>
            
            <form action="{{ route('stu.destroy', $d->id) }}" method="post">
                <a href="{{ route('stu.show', $d->id) }}" class="btn btn-primary">Detail</a>
            <a href="{{ route('stu.edit', $d->id) }}" class="btn btn-success">Edit</a>
                @csrf
                @method('delete')
                <input value="Hapus" type="submit" class="btn btn-danger">
            </form>
        @endforeach
    </div>
@endsection