@extends('layouts.main')

@section('title', 'Show | Official Latihan 1')

@section('container')
    <div class="container">
        <a href="{{ route('stu.index') }}">Back</a>
        <h1>Judul : {{ $stu->judul }}</h1>
        <h3>Penulis : {{ $stu->penulis }}</h3>
        <p>{{ $stu->teks }}</p>
    </div>
@endsection