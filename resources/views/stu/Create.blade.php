@extends('layouts.main')

@section('title', 'Tambah | Official Latihan 1')

@section('container')
    <div class="container mt-5">
        <h3>Penambahan Konten Stu</h3>
        <form action="/stu" method="POST">
            @csrf
            <div class="mb-3">
                <label for="judul" class="form-label">Judul</label>
                <input type="text" class="form-control @error('judul') is-invalid @enderror" value="{{old('judul')}}" id="judul" name="judul">
                @error('judul')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="penulis" class="form-label">Penulis</label>
                <input type="text" class="form-control" id="penulis" name="penulis" value="{{old('penulis')}}">
            </div>
            <div class="mb-3">
                <label for="teks" class="form-label">Teks</label>
                <input type="text" class="form-control" id="teks" name="teks" value="{{old('teks')}}">
            </div>
            {{-- <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Check me out</label>
            </div> --}}
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection