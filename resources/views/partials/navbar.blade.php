<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="/">Rheks</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ url('/') }}" class="nav-link active" aria-current="page">Home</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/about') }}" class="nav-link">About</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/pemakai') }}" class="nav-link">Pemakai</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/stu') }}" class="nav-link">Stu</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link disabled" tabindex="-1" aria-disabled="true">Disabled</a>
                </li> -->
            </ul>
        </div>
    </div>
</nav>