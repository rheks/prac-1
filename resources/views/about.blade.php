@extends('layouts.main')

@section('title', $title)

@section('container')
    <div class="container">
        <h3>{{ $nama }}</h3>
        <img src="img/{{ $gambar }}" width="30px" height="30px">
    </div>
@endsection

