@extends('layouts.main')

@section('title', $title)

@section('container')
    <div class="container">
        <h1>{{ $content }}</h1>
    </div>
        
    <div id="Index"></div>
@endsection