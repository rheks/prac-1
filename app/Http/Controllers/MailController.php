<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\testMail;
use Illuminate\Mail\Mailer;

class MailController extends Controller
{
    public function sendEmail(){
        $details = [
            'title' => 'Judul',
            'body' => "Testing send email with Laravel 8"
        ];
        Mail::to('1811503083@student.budiluhur.ac.id')->send(new testMail($details));
        return 'Email Berhasil Terkirim Lagi 2';
    }
}
