<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Stu;

class StuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Stu::all();
        return view('stu.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stu.Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul'=> 'required|size:3',
            'penulis'=> 'required',
            'teks'=> 'required'
        ]);

        Stu::create($request->all());

        return redirect()->route('stu.index')->with('status', 'Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Stu $stu)
    {
        return view('stu.show', compact('stu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Stu $stu)
    {
        return view('stu.edit', compact('stu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stu $stu)
    {
        $request->validate([
            'judul'=> 'required',
            'penulis'=> 'required',
            'teks'=> 'required'
        ]);

        $stu->update($request->all());

        return redirect()->route('stu.index')->with('status', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stu $stu)
    {
        $stu->delete();

        return redirect()->route('stu.index')->with('status', 'Data berhasil dihapus');
    }
}
