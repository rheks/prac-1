<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Pengguna extends Controller
{
    public function index() {
        $title = "Homepage | Official Latihan 1";
        $content = "Selamat Datang di Homepage";
        return view('index', [
            'title' => $title,
            'content' => $content
        ]);
    }

    public function about() {
        $title = "About | Official Latihan 1";
        $nama = "Rheks";
        $gambar = "favicon.jpg";

        return view('about', [
            'title' => $title,
            'nama' => $nama,
            'gambar' => $gambar
        ]);
    }

    public function pemakai() {
        $data = [
            [
                "title" => "Judul Pertama",
                "author" => "Penulis Pertama",
                "teks" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore amet dignissimos dolor 
                reprehenderit aliquid error impedit doloribus ipsam, facere similique, doloremque mollitia nemo? 
                Assumenda dolorem eaque id. Debitis neque in sapiente cumque, fugiat saepe autem atque ipsam accusamus
                ores consectetur a. Quasi qui vel assumenda dolorum vitae nostrum, in earum molestias iusto ipsum at 
                repudiandae atque perferendis, ut inventore. Similique veritatis consequuntur numquam? Ut reiciendis 
                perspiciatis non laboriosam corrupti explicabo illo cumque, error atque maiores. Corrupti voluptate 
                minus iusto accusamus."
            ],
            [
                "title" => "Judul Kedua",
                "author" => "Penulis Kedua",
                "teks" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore amet dignissimos dolor 
                reprehenderit aliquid error impedit doloribus ipsam, facere similique, doloremque mollitia nemo? 
                Assumenda dolorem eaque id. Debitis neque in sapiente cumque, fugiat saepe autem atque ipsam accusamus
                ores consectetur a. Quasi qui vel assumenda dolorum vitae nostrum, in earum molestias iusto ipsum at 
                repudiandae atque perferendis, ut inventore. Similique veritatis consequuntur numquam? Ut reiciendis 
                perspiciatis non laboriosam corrupti explicabo illo cumque, error atque maiores. Corrupti voluptate 
                minus iusto accusamus."
            ],
            [
                "title" => "Judul Ketiga",
                "author" => "Penulis Ketiga",
                "teks" => "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Inventore amet dignissimos dolor 
                reprehenderit aliquid error impedit doloribus ipsam, facere similique, doloremque mollitia nemo? 
                Assumenda dolorem eaque id. Debitis neque in sapiente cumque, fugiat saepe autem atque ipsam accusamus
                ores consectetur a. Quasi qui vel assumenda dolorum vitae nostrum, in earum molestias iusto ipsum at 
                repudiandae atque perferendis, ut inventore. Similique veritatis consequuntur numquam? Ut reiciendis 
                perspiciatis non laboriosam corrupti explicabo illo cumque, error atque maiores. Corrupti voluptate 
                minus iusto accusamus."
            ]
        ];

        return view("pemakai", compact('data'));
    }
}