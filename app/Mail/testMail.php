<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class testMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com', 'Dahlah capek 2')->subject('Testing Mail Using Laravel 8')->view('emails.testMail');

        // Add this to .env

        // MAIL_MAILER=smtp
        // MAIL_HOST=smtp.googlemail.com
        // MAIL_PORT=465
        // MAIL_USERNAME=andre.ktsr9@gmail.com
        // MAIL_PASSWORD=rahasia090600
        // MAIL_ENCRYPTION=ssl
        // MAIL_FROM_ADDRESS=
        // MAIL_FROM_NAME="${APP_NAME}"

        // Setting less secure app access into 'On"
    }
}
