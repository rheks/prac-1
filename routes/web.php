<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Pengguna;
use App\Http\Controllers\StuController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', 'Pengguna@index'); // udah gak bisa dipake

Route::get('/', [Pengguna::class, 'index']);

Route::get('/about', [Pengguna::class, 'about']);

Route::get('/pemakai', [Pengguna::class, 'pemakai']);

Route::resource('/stu', StuController::class);

Route::get('/sendmail', [MailController::class, 'sendEmail']);